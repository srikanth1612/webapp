from django.shortcuts import render
from Demo1.forms import userforms
# Create your views here.
def demo(request):
    # d1 = {
    #     'name':'srikanth' ,
    #     's1' : [1,2,3,4,5,6],
    #     's2' : {'address':'saguturu', 'city': 'nellore' }
    #
    #
    # }

    obj = userforms()
    # print(request.POST)

    if request.method == 'POST':
        obj = userforms(request.POST)
        if obj.is_valid():
            pass

    return render(request , 'myDemo.html' , {'form':obj})