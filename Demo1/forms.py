from django import forms

class userforms(forms.Form):

    dropData = (
        ('','please select category') ,
        ('hyderabad' , 'Hyderabad'),
        ('chennai' ,'Chennai') ,
        ('banglore' , 'Banglore')
    )

    city = forms.ChoiceField(choices=dropData)


    radioData = (
        ('m', 'Male'),
        ('f' ,'Female')
    )

    Gender = forms.ChoiceField(choices=radioData , widget=forms.RadioSelect)


    booling = forms.BooleanField(required=True)

    booling2 = forms.MultipleChoiceField(choices=dropData , widget=forms.CheckboxSelectMultiple)

    username = forms.CharField(required=True , label= 'Name' , max_length=10 , min_length=2 , help_text='do something ' ,
                                error_messages= {
                                    'required': 'this is it',
                                    'min_length':'settings'

                                } , widget = forms.TextInput(attrs={
        'placeHolder':'userName'
    }) )

    email = forms.EmailField()
    adress = forms.CharField()

    def clean(self):
        formData = self.cleaned_data

        if 'username' in formData and formData ['username'].isdigit():
            self.errors['username'] = ['user name cant have numbers']

            return formData

