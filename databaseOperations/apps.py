from django.apps import AppConfig


class DatabaseoperationsConfig(AppConfig):
    name = 'databaseOperations'
