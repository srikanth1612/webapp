from django.shortcuts import render,redirect
from databaseOperations.forms import userforms
from databaseOperations.models import College
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
def demo(request):

    obj = userforms()
    print(request.POST)

    if request.method == 'POST':
        obj = userforms(request.POST)
        if obj.is_valid():
            pass

    return render(request, 'mydemo2.html', {'form': obj})

@login_required(login_url='/login')
def create(request):
    obj1 = userforms()

    if request.method == 'POST' :
        form = userforms(request.POST)

        if form.is_valid():

            clg = College()
            clg.username = form.cleaned_data['username']
            clg.address = form.cleaned_data['address']
            clg.email = form.cleaned_data['email']
            clg.save()
            return redirect(index)
    return render(request, 'dbfiles/create.html', {'form': obj1})

@login_required(login_url='/login')
def update(request):

    id = request.GET['id']

    form = userforms()

    if request.method == 'POST' :
        form = userforms(request.POST)
        if form.is_valid():
            clg = College()
            clg.id = id
            clg.username = form.cleaned_data['username']
            clg.address = form.cleaned_data['address']
            clg.email = form.cleaned_data['email']
            clg.save()
            return redirect(index)
    result = College.objects.get(id=id)
    print(result.username)
    form.fields['username'].initial = result.username
    form.fields['address'].initial = result.address
    form.fields['email'].initial = result.email
    return render(request , 'dbfiles/update.html' , {'form':form})

@login_required(login_url='/login')
def index(request):
    results = College.objects.all()

    page = request.GET.get('page', 1)

    paginator = Paginator(results, 2)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)

    # return render(request, 'core/user_list.html', {'users': users})
    return render(request , 'dbfiles/index.html' , {'users': users})

@login_required(login_url='/login')
def view(request):
    id = request.GET['id']

    form = userforms()

    if request.method == 'POST':
        form = userforms(request.POST)

    result = College.objects.get(id=id)


    return render(request, 'dbfiles/view.html', { 'result':result})

@login_required(login_url='/login')
def delete(request):

    id = request.GET['id']
    result = College.objects.get(id=id)
    result.delete()
    return redirect(index)
