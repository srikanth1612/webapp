from django.db import models

# Create your models here.

class College(models.Model):
    username = models.CharField(max_length=250)

    email = models.EmailField()
    address = models.CharField(max_length=200)