from django.conf.urls import url
from databaseOperations import views

urlpatterns = [
    url(r'^hello-data$', views.demo),
    url(r'^hello-create$', views.create),
    url(r'^hello-update$', views.update),
    url(r'^hello-index$', views.index),
    url(r'^hello-view$', views.view),
    url(r'^hello-delete$', views.delete)

]