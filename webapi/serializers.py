from rest_framework import serializers


class MobileSerializer(serializers.Serializer):

    id = serializers.IntegerField(required=False)
    mobilename = serializers.CharField()
    brand = serializers.CharField()
    model = serializers.CharField()
    batteryCapacity = serializers.IntegerField()

    def validate(self, data):
        if data['mobilename'].isdigit():

            raise serializers.ValidationError("name field cannot be numbers")
        return data