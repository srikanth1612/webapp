from django.db import models

# Create your models here.


class MobileStock(models.Model):

    mobilename = models.CharField(max_length=20)
    brand = models.CharField(max_length=20)
    model = models.CharField(max_length=20)
    batteryCapacity = models.IntegerField()

    class Meta:
        db_table = 'mobiles'
