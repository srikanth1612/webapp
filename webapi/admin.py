from django.contrib import admin
from webapi.models import MobileStock
from django import forms
# Register your models here.


class MobileForm(forms.ModelForm):
    class Meta:
        model = MobileStock
        fields = "__all__"

    def clean_mobilename(self):
        if self.cleaned_data['mobilename'].isdigit():
            raise forms.ValidationError("mobile name cannot be no's")
        return self.cleaned_data['mobilename']


class MobileAdmin(admin.ModelAdmin):
    form = MobileForm


admin.site.register(MobileStock, MobileAdmin)