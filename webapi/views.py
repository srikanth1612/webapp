from django.shortcuts import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from webapi.models import MobileStock
from webapi.serializers import MobileSerializer

# Create your views here.
class AddMobileSpec(APIView):
    def get(self,request):
        empread = MobileStock.objects.all()
        serilazer = MobileSerializer(empread,many=True)
        return Response(serilazer.data)


    def post(self,request):

        serializer = MobileSerializer(data=request.data)
        if serializer.is_valid():
            mobilestock = MobileStock()
            mobilestock.mobilename = request.data['mobilename']
            mobilestock.brand = request.data['brand']
            mobilestock.model = request.data['model']
            mobilestock.batteryCapacity = request.data['batteryCapacity']
            mobilestock.save()
            return Response(serializer.data ,status=status.HTTP_201_CREATED)
        return Response(serializer.errors , status=status.HTTP_400_BAD_REQUEST)


class MobileDetail(APIView):
    def get_object(self,id):
        try:
            return MobileStock.objects.get(id=id)
        except MobileStock.DoesNotExist:
            raise Http404

    def get(self,request,pk):
        obj = self.get_object(pk)
        obj1 = MobileSerializer(obj)
        return Response(obj1.data)

    def put(self,request,pk):
        apple=MobileSerializer(data=request.data)
        if apple.is_valid():
            mobilestock = MobileStock.objects.get(id=pk)
            mobilestock.mobilename = request.data['mobilename']
            mobilestock.brand = request.data['brand']
            mobilestock.model = request.data['model']
            mobilestock.batteryCapacity = request.data['batteryCapacity']
            mobilestock.save()
            return Response(apple.data, status=status.HTTP_201_CREATED)
        return Response(apple.errors, status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,pk):

        obj = self.get_object(id=pk)
        if obj.delete():
            data = {
                'message' : 'deleted succesfully',
                'id': pk
            }
            return Response(data)

        return Response("something went wrong")











