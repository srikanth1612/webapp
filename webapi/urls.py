from django.conf.urls import url
from webapi import views

urlpatterns = [
    url(r'^mobile$', views.AddMobileSpec.as_view()),
    url(r'mobile/(?P<pk>[0-9]+)/$', views.MobileDetail.as_view())
]