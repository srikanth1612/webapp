from django.conf.urls import url
from sites import views

urlpatterns = [
    url(r'^login$', views.login),
    url(r'^home$', views.home),
    url(r'^registration$', views.registration),
    url(r'^logout$' , views.logout),
    url(r'^staticexample$' , views.staticExample),
url(r'^classlogin$' , views.classBaseView.as_view())


]