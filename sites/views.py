from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User as MainUser
from sites.forms import LoginForm
from django.contrib.auth import login as userlogin , logout as userlogout , authenticate
from databaseOperations.models import College
from django.db.models import Q
from django.views import View


# Create your views here.



class classBaseView(View):

    def __init__(self):
        self.form = LoginForm()
        self.message = ""

    def get(self,request):
        return render(request , 'Auth/login.html', {'form':self.form})
    def post(self,request):

            self.form = LoginForm(request.POST)

            if self.form.is_valid():
                username = self.form.cleaned_data['username']
                password = self.form.cleaned_data['password']

                user = authenticate(username=username, password=password)

                if user is None:
                    self.message = "Invalid Login Details"
                else:
                    userlogin(request, user)
                    request.session['phone'] = 123123123
                    request.session['city'] = 'banglore'
                    return redirect(home)

            return render(request, 'Auth/login.html', {'form': self.form, 'message': self.message})


def login(request):

    if request.user.username:
        return redirect(home)

    message = ""
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username= username, password=password)

            if user is None:
                message = "Invalid Login Details"
            else :
                userlogin(request,user)
                request.session['phone'] = 123123123
                request.session['city'] = 'banglore'
                return redirect(home)

    return render(request, 'Auth/login.html' , {'form':form , 'message':message})


def home(request):
    return render(request, 'Auth/home.html')


def registration(request):



    result = College.objects.filter(Q(id=15)|Q(id=2)).values("id")
    # result = College.objects.raw()
    print(result)


    if request.user.username:
       return redirect(home)

    user = UserCreationForm()
    message = ''

    if request.method == 'POST':
        user = UserCreationForm(request.POST)
        if user.is_valid():
            main = MainUser()
            main.username = user.cleaned_data['username']
            main.set_password(user.cleaned_data['password1'])
            main.save()
            message = 'saved successfully'
            return redirect(login)

    return render(request, 'Auth/registration.html', {'form': user, 'message': message})

def logout(request):
    userlogout(request)
    return redirect(login)



def staticExample(request):
    return render(request, "Auth/staticexample.html")